// use nix::net::if_::{Interface, Interfaces, InterfacesIter, if_nametoindex};
// use nix::ifaddrs::{getifaddrs, InterfaceAddress, InterfaceAddressIterator};
use futures::stream::TryStreamExt;
use rtnetlink::{Error, Handle, IpVersion, new_connection, packet::{nlas::Nla}, };
use std::net::IpAddr;
use std::net::Ipv6Addr;
use std::net::Ipv4Addr;

#[tokio::main]
async fn main() -> Result<(), ()> {
    // let addrs = nix::ifaddrs::getifaddrs().unwrap();
    // for ifaddr in addrs {
    //     match ifaddr.address {
    //         Some(address) => {
    //         println!("interface {} address {}",
    //                 ifaddr.interface_name, address);
    //         },
    //         None => {
    //         println!("interface {} with unsupported address family",
    //                 ifaddr.interface_name);
    //         }
    //     }
    // }

    println!("interfaces:");
    let interfaces = nix::net::if_::if_nameindex().unwrap();
    for iface in &interfaces {
        println!("{}: {}", iface.index(), iface.name().to_string_lossy());
    }   

    println!();

    let (connection, handle, _) = new_connection().unwrap();
    tokio::spawn(connection);

    //let link = "lo".to_string();
    //println!("getting address for link \"{}\"", link);
    // if let Err(e) = dump_addresses(handle.clone(), link).await {
    //     eprintln!("{}", e);
    // }

    println!("neighbors:");
    if let Err(e) = get_neighbours(handle.clone()).await {
        eprintln!("{}", e);
    }

    Ok(())
}

async fn get_addresses(handle: Handle, link: String) -> Result<(), Error> {
    let mut links = handle.link().get().set_name_filter(link.clone()).execute();
    if let Some(link) = links.try_next().await? {
        let mut addresses = handle
            .address()
            .get()
            //.set_link_index_filter(link.header.index)
            .execute();
        while let Some(msg) = addresses.try_next().await? {
            println!("{:?}", msg);
        }
        Ok(())
    } else {
        eprintln!("link {} not found", link);
        Ok(())
    }
}

async fn get_neighbours(handle: Handle) -> Result<(), Error> {
    let mut neighbours = handle
        .neighbours()
        .get()
        //.set_family(IpVersion::V4)
        .execute();

    while let Some(route) = neighbours.try_next().await? {
        println!("{:?}", route.header);
    
        for nla in route.nlas {
            //println!("{:?}, ", nla);

            let mut addr:IpAddr = IpAddr::V4(Ipv4Addr::from([0,0,0,0]));

            if nla.kind() == 1 {

                //if route.header.family == IpVersion::V6 as u8 {
                if route.header.family == 10 {
                    let mut arr6:[u8;16] = [0;16];
                    nla.emit_value(&mut arr6);
                    addr = IpAddr::V6(Ipv6Addr::from(arr6));
                } else if route.header.family == 2 {
                    let mut arr4:[u8;4] = [0;4];
                    nla.emit_value(&mut arr4);
                    addr = IpAddr::V4(Ipv4Addr::from(arr4));
                }

                println!("{:?}%{:?}, ", addr, route.header.ifindex);
            } 
        }
    }
    Ok(())
}